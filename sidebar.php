<div class="col-xs-12 col-sm-12 col-md-3">
        <aside class="sidebar">
          <div class="widget widget-search ">
            <h3 class="widget-title"><span>Search Blog</span></h3>
              <?php get_search_form(); ?>
          </div>
          
          <!-- CATEGORY LIST WIDGET -->
          <div class="widget widget-category">
            <h3 class="widget-title"><span>categories</span></h3>
              <?php echo get_the_category_list(); ?>
          </div>
          <!-- // CATEGORY LIST WIDGET --> 
          
          <!-- TABBED CONTENT WIDGET -->
          <div class="widget widget-tabbed">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab-popular" data-toggle="tab">popular</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab-popular">
                <ul class="entry-list unstyled">
                  <li>
                      <?php $args = array(
                    'orderby' => 'title');
                      $the_spost = get_posts($args);
                      
                      if($the_spost) : foreach($the_spost as $post): setup_postdata($post); ?>
                    <div class="entry-thumbnail">
                       <?php if(has_post_thumbnail($post->ID)):
                        $the_image = wp_get_attachment_image_src(get_post_thumbnail_id( $post->ID), 'single-post-thumbnail' ); ?>
                        <a href="<?php echo $the_image[0]; ?>" class="img"> <img src="<?php echo $the_image[0]; ?> " width="70" height="70" alt=""/></a> 
                      <?php else: ?>
                       <a href="<?php the_permalink(); ?>" class="img"> <img src="<?php echo get_template_directory_uri(); ?>/media/small/1.jpg ?> " width="70" height="70" alt=""/></a> 
                        <?php endif; ?>
                        
                      </div>
                    <div class="entry-main">
                      <div class="entry-meta">
                        <time class="entry-datetime" datetime="<?php get_the_date(get_option('date_format'));?>" title="<?php get_the_date(get_option('date_format'));?>"> <a href="<?php the_permalink();?>"> <?php echo get_the_date(get_option('date_format'));?><span aria-hidden="false" class="icon-clock"></span> <?php get_the_date(get_option('date_format'));?></a> </time>
                      </div>
                      <div class="entry-header">
                        <h5 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                  </li>
                  <?php endforeach; endif; ?>
                </ul>
              </div>
            </div>
          </div>
          <!-- // TABBED CONTENT WIDGET --> 
          
          <!-- FLICKR STREAM -->
          <div class="widget widget-flickr">
            <h3 class="widget-title"><span>FLICKR photos</span></h3>
            <ul id="flickr-feed" class="cells-9 img unstyled clearfix" data-items="9">
            </ul>
          </div>
          <!-- // FLICKR STREAM --> 
          
          <!-- TAGS WIDGET -->
          <div class="widget widget-tags">
            <h3 class="widget-title"><span>Tags cloud</span></h3>
            <ul class="tag-cloud unstyled clearfix">
                <?php
                    $t_args = array(
                        'number' => '10',
                        'hide_empty' => true,
                        'orderby' => 'count'
                    );
                    $tags = get_tags($t_args);
                
                foreach ($tags as $tag):
                ?>
              <li><a href="<?php echo get_tag_link($tag->term_id); ?>"><?php echo $tag->name ?></a></li>
                <?php endforeach; ?>
            </ul>
          </div>
          <!-- // TAGS WIDGET --> 
          
          <!--  WIDGET -->
          <div class="widget widget-latest-post">
            <h3 class="widget-title"><span>LATEST POSTS</span></h3>
            <ul class="carousel1">
                <?php
                  $l_args = array(
                    'orderby' => 'date'
                  );
                  $l_post = get_posts($l_args);
                  foreach ($l_post as $post):
                  ?>
                  
              <li>
                <div class="media">
                    <?php
                        if(has_post_thumbnail($post->ID)):
                            $the_f_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail');
                    ?><a href="<?php the_permalink(); ?>"> <img src="<?php echo $the_f_image[0]; ?>" width="270" height="190" alt="<?php the_title(); ?>"/></a>
                        <?php else: ?>
                    <a href="<?php the_permalink(); ?>"> <img src="<?php echo  get_template_directory_uri(); ?>/media/small/1.jpg" width="270" height="190" alt="<?php the_title(); ?>"/></a>
                    <?php endif; ?>
                  <div class="media-desc">
                    <h5 class="entry-title"><?php the_title(); ?></h5>
                    <time class="entry-datetime" datetime="<?php echo get_the_date(get_option('date_format')); ?>" title="<?php echo get_the_date(get_option('date_format')); ?>"> <a href="#"> <i class="fa fa-clock-o"></i> <?php echo get_the_date(get_option('date_format')); ?></a> </time>
                  </div>
                </div>
                </li>
                 <?php endforeach; ?>
              </ul>
            </div>
   <!-- //  WIDGET --> 
          
        </aside>
      </div>