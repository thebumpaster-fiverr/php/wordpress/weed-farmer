<!DOCTYPE html>
<!-- Author Ismar Hadzic hadzicismar67@gmail.com -->
<?php wp_head(); ?>
<html lang="en" style="margin-top:0px !important; ">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title(); ?></title>
<!--[if lt IE 9]>
			<script src="js/html5shiv-3.7.0.js"></script>
			<script src="js/respond-1.4.2.js"></script>
		<![endif]-->
</head>
<?php if(is_front_page()): ?>
<body class="noIE home">
  <?php else: ?>
  <body class="noIE inner-page">
  <?php endif; ?>
</div>
<div id="loader-wrapper">
    <div id="loader"></div>
</div>
<!-- HEADER -->
<header id="main-header">
  <div id="cd-nav"> <a href="#0" class="cd-nav-trigger">Menu<span></span></a>
    <nav id="cd-main-nav">
		<?php
		wp_nav_menu( array( 
			  'theme_location' => 'header-menu',
			  'depth' => '5',
			  'link_before' => '<span>',
			  'link_after' => '</span>',
			  )); 
		?>
	</nav>
  </div>
  <div class="container ">
    <div class="row">
      <div class="col-md-3 spacer"> </div>
      <div class="col-md-6"> 
        <!-- NAVIGATION -->
		<?php if( is_front_page()): ?>
        <div class="navbar navbar-default navbar-top ">
          <div class="navbar-collapse">
			<?php
			wp_nav_menu( array(
				'theme_location' => 'header-menu',
				'depth' => '5', 'before' => '<li>',
				'after' => '</li>',
				'link_before' => '<span>',
				'link_after' => '</span>',
				'menu_class' => 'nav navbar-nav nav-justified'
				)); 
			?>
          </div>
        </div>
		<?php endif; ?>
      </div>
      <div class="col-md-4"> 
        <!-- LOGO -->
        <div id="logo" > <span class="bglogo1 transform-please-2"><a href="<?php echo esc_url(home_url('/')) ?>"><img src="<?php echo get_template_directory_uri()?>/img/logo.png"  alt="logo"/></a></span> <span class="bglogo2 transform-please-2"></span> <span class="bglogo3 transform-please-2"></span> <span class="bglogo4 transform-please-2"></span> </div>
      </div>
    </div>
  </div>
  
  <div class="container">
    <nav id="main-nav" >
      <div class="row">
        <div class="col-md-12"> 
          <!-- NAVIGATION -->
          <div class="navbar navbar-default" role="navigation">
            <div class="navbar-header"> </div>
            <div class="navbar-collapse collapse">
			  
			  <?php 
			  
			  wp_nav_menu( array( 
			  'theme_location' => 'extra-menu',
			  'depth' => '5',
			  'link_before' => '<span>',
			  'link_after' => '</span><small>Extra text</small>',
			  'menu_class' => 'nav navbar-nav nav-justified',
			  'walker' => new WPDocs_Walker_Nav_Menu()
			  )); 
			  ?>
         
			  
            </div>
            <div class="mini-search-top">
              <form action="<?php bloginfo('url'); ?>/" class="form-mini-search" id="searchform" method="get" role="search" >
                <input type="text" id="s" name="s" value="<?php the_search_query(); ?>" placeholder="Search..">
                <button id="searchsubmit"  type="submit" class="arrow-link submit-link"> <span class="icon-transform transform-please"><i class="fa fa-search"></i></span></button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </nav>
    <div class="hot-news col-md-6  transform-revers-2">
      <h5 class="themecolor1"> <a href="#">LEAFLY SAYS...</a></h5>
  <div class="marquee"><a href="#"> Something smart :D </a></div>
    </div>
  </div>
</header>
<?php if(is_front_page() ) : ?>
<div id="slider">
  <div class="camera_slider">
    <div data-thumb="<?php echo get_template_directory_uri()?>/media/slider/1.jpg" data-src="<?php echo get_template_directory_uri()?>/media/slider/1.jpg"> </div>
    <div data-thumb="<?php echo get_template_directory_uri()?>/media/slider/2.jpg" data-src="<?php echo get_template_directory_uri()?>/media/slider/2.jpg"> </div>
    <div data-thumb="<?php echo get_template_directory_uri()?>/media/slider/3.jpg" data-src="<?php echo get_template_directory_uri()?>/media/slider/3.jpg"> </div>
    <div data-thumb="<?php echo get_template_directory_uri()?>/media/slider/4.jpg" data-src="<?php echo get_template_directory_uri()?>/media/slider/4.jpg"> </div>
  </div>
</div>
 <?php endif; ?>
<!-- HEADER END --> 