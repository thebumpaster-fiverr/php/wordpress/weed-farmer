
<!-- FOOTER -->
<div class="col-xs-12 col-sm-12 col-md-12">
<div class="footer-block">
  <div class="container">
    <div class="row">
      <div class="foot-top-line"></div>
      <div class="foot-bot-line"></div>
      <a class="scroll-top" href="#"><i class="fa fa-angle-up"></i> </a> </div>
  </div>
  <div class="container fot-container">
    <div class="row">
	   <?php wp_nav_menu( array( 'theme_location' => 'footer-menu' , 'depth' => '5', 'before' => '<li>', 'after' => '</li>', 'link_before' => '<span>', 'link_after' => '</span>', 'menu_class' => 'nav-footer') ); ?> 
    </div>
  </div>
  <div class="container">
    <div class="row text-center copyright-info">
      <p> © ALL RIGHTS RESERVED 2016 - WEEDFARMER</p>
    </div>
  </div>
</div>
<!-- END --> 

<!-- ABSOLUTE FOOTER -->
<div class="copyright-block">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 left">
        <ul class="unstyled clearfix social-team">
          <li><a href="http://fb.com/#" target="_blank"><i class="fa fa-facebook"></i></a></li>
          <li><a href="http://twitter.com/#" target="_blank"><i class="fa fa-twitter"></i></a></li>
          <li><a href="http://plus.google.com/#" target="_blank"><i class="fa fa-google"></i></a></li>
          <li><a href="http://youtube.com/#" target="_blank"><i class="fa fa-youtube"></i></a></li>
          <li><a href="http://rss.com/#" target="_blank"><i class="fa fa-rss"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!-- END --> 
</div>