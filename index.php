<?php get_header(); ?>

      <!-- MAIN CONTENT -->
      <!-- SECTION -->
<div id="m-content" class="section-3" >
  <div class="container">
    <div class="row" >
     <section class="carousel carousel-reviews">
        <div class="carousel-title ">
          <div class="transform-please-2 "> <span> STRAIN REVIEWS </span> </div>
        </div>
        <ul class="carousel-1">
            <?php 
            $args = array( 'posts_per_page' => 20, 'category_name'    => 'reviews');
            $the_post = get_posts($args); 

            if($the_post): foreach($the_post as $post) : setup_postdata($post);  ?>
          <li>
            <div class="media">
                <?php if(has_post_thumbnail($post->ID)): ?>
                <?php $the_f_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                
                <a href="<?php the_permalink(); ?>"><img src="<?php echo $the_f_image[0]; ?>" width="369" height="200" alt="alt"/></a>
                <?php else : ?>
                    <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/media/blog/1.jpg" width="369" height="200" /> </a>
                <?php endif; ?>
              <div class="carousel-item-content">
                <div class="text-right"><a class="arrow-link" href="#"> <span class="icon-transform transform-please-2"><i class="fa fa-angle-right"></i></span></a></div>
                <a href="<?php the_permalink(); ?>" class="transform-please-2 carousel-title"><span><?php the_title(); ?></span> </a> </div>
            </div>
            <div class="carousel-text">
              <p><?php the_excerpt(); ?> </div>
            <div class="box-more-info">
              <div class="transform-revers"> <a href="#"><?php echo get_the_date(get_option('date_format')); ?></a></div>
            </div>
          </li>
        <?php endforeach; else: ?>
	       <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                <?php wp_reset_postdata(); endif;?>
        </ul>
      </section>
    </div>
  </div>
</div>
<!--END--> 
<div class="space40 visible-xs"></div>

    <div class="col-xs-12 col-sm-12 col-md-9">
        <section class="main-content" role="main">
            <?php 
            
            $the_post = get_posts($args); 

            if($the_post): foreach($the_post as $post) : setup_postdata($post);  ?>
          <article class="post format-image animated" data-animation="bounceInLeft">
              
            <div class="entry-header">
              <div class="box-more-info">
                <div class="transform-revers"> <a href="#"> LATEST</a></div>
                <div class="transform-revers"> <a href="#"> <i class="fa fa-clock-o"></i><?php echo get_the_date(get_option('date_format')); ?></a></div>
              </div>
            </div>
            <div class="entry-media">
              <div class="entry-thumbnail img">
                  
                  <?php if(has_post_thumbnail($post->ID)): ?>
                <?php $the_f_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                  
                <a href="<?php echo $the_f_image[0]; ?>"> <img src="<?php echo $the_f_image[0]; ?>" width="870px" height="300px" alt="<?php the_title();?>"/></a>
                  
                <?php else: ?>
                  
                  <a href="<?php echo get_template_directory_uri();?>/media/blog/1.jpg"> <img src="<?php echo get_template_directory_uri();?>/media/blog/1.jpg" width="870px" height="300px" alt="<?php the_title();?>"/></a>
                </div>
                <?php endif;?>
            </div>
            <div class="entry-main">
              <h3 class="entry-title"> <a href="<?php the_permalink();?>" data-hover="<?php the_title(); ?>"><?php the_title(); ?></a> </h3>
              <div class="entry-content">
                <p><?php the_excerpt(); ?> <a href="<?php the_permalink(); ?>" class="readmore grow" title="Continue Reading">[...]</a></p>
                <div class="entry-footer"> <a href="<?php the_permalink(); ?>" class="arrow-link"><span class="vm-text">View more</span> <span class="icon-transform transform-please-2"><i class="fa fa-angle-right"></i></span></a> </div>
              </div>
            </div>
            <div class="line-block transform-please-2"></div>
          </article>
            <?php endforeach; else: ?>
            <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                <?php  wp_reset_postdata();  endif; ?>
          <!--<nav class="pagination">
            <ul>
              <li class="active"><a href="#" class="btn btn-primary"><span>1</span></a></li>
              <li><a href="#" class="btn btn-default">2</a></li>
              <li><a href="#" class="btn btn-default">3</a></li>
              <li><a href="#" class="btn btn-default">4</a></li>
              <li><a href="#" class="btn btn-default">5</a></li>
            </ul>
          </nav> -->
        </section>
      </div>
       <div class="space40 visible-xs"></div>
	  <?php get_sidebar(); ?>
<div class="space40 visible-xs"></div>
<?php get_footer(); ?>