<?php define( 'WP_USE_THEMES', false ); get_header(); ?>
<!-- HOME SLIDER -->
<!-- SECTION -->
<div id="m-content" class="section-1 animate-paraslide-image animatedEntrance">
  <div class="container">
    <div class="row">
      <div class="col-md-offset-2 col-md-9">
        <header class="section-header ">
          <div class="heading-wrap">
            <h2 class="heading"><span> WELCOME TO THE WORLD OF CANABIS</span></h2>
          </div>
            <p> Our community is about helping and improving your growing skills, as well as some funny pictures/videos, and let’s not forget our tips and tricks for your very own indoor/outdoor growing style. If you have any further questions, please feel free to message us, we will be sure to respond in the quickest way possible.</p>
        </header>
      </div>
    </div>
  </div>
</div>
<!-- END --> 

<!-- SECTION -->
<div class="section-3" >
	
	
  <div class="container">
    <div class="row" >
      <section class="carousel carousel-reviews">
        <div class="carousel-title ">
          <div class="transform-please-2 "> <span> STRAIN REVIEWS </span> </div>
        </div>
        <ul class="carousel-1">
            <?php 
            $args = array( 'posts_per_page' => 20, 'category_name'    => 'reviews');
            $the_post = get_posts($args); 

            if($the_post): foreach($the_post as $post) : setup_postdata($post);  ?>
          <li>
            <div class="media">
                <?php if(has_post_thumbnail($post->ID)): ?>
                <?php $the_f_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                
                <a href="<?php the_permalink(); ?>"><img src="<?php echo $the_f_image[0]; ?>" width="369" height="200" alt="alt"/></a>
                <?php else : ?>
                    <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/media/blog/1.jpg" width="369" height="200" /> </a>
                <?php endif; ?>
              <div class="carousel-item-content">
                <div class="text-right"><a class="arrow-link" href="#"> <span class="icon-transform transform-please-2"><i class="fa fa-angle-right"></i></span></a></div>
                <a href="<?php the_permalink(); ?>" class="transform-please-2 carousel-title"><span><?php the_title(); ?></span> </a> </div>
            </div>
            <div class="carousel-text">
              <p><?php the_excerpt(); ?> </div>
            <div class="box-more-info">
              <div class="transform-revers"> <a href="#"><?php echo get_the_date(get_option('date_format')); ?></a></div>
            </div>
          </li>
        <?php endforeach; else: ?>
	       <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                <?php wp_reset_postdata(); endif;?>
        </ul>
      </section>
        <br><br>
         <section class="carousel carousel-reviews">
        <div class="carousel-title ">
          <div class="transform-please-2 "> <span> BOTANICS </span> </div>
        </div>
        <ul class="carousel-1">
            <?php 
            $args = array( 'posts_per_page' => 20, 'category_name'    => 'botanics' );
            $the_post = get_posts($args); 

            if($the_post): foreach($the_post as $post) : setup_postdata($post);  ?>
          <li>
            <div class="media">
                <?php if(has_post_thumbnail($post->ID)): ?>
                <?php $the_f_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                
                <a href="<?php the_permalink(); ?>"><img src="<?php echo $the_f_image[0]; ?>" width="369" height="200" alt="alt"/></a>
                <?php else : ?>
                    <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/media/blog/1.jpg" width="369" height="200" /> </a>
                <?php endif; ?>
              <div class="carousel-item-content">
                <div class="text-right"><a class="arrow-link" href="#"> <span class="icon-transform transform-please-2"><i class="fa fa-angle-right"></i></span></a></div>
                <a href="<?php the_permalink(); ?>" class="transform-please-2 carousel-title"><span><?php the_title(); ?></span> </a> </div>
            </div>
            <div class="carousel-text">
              <p><?php the_excerpt(); ?> </div>
            <div class="box-more-info">
              <div class="transform-revers"> <a href="#"><?php echo get_the_date(get_option('date_format')); ?></a></div>
            </div>
          </li>
        <?php endforeach; else: ?>
	       <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                <?php wp_reset_postdata(); endif;?>
        </ul>
      </section>
        
    </div>
  </div>
</div>

<div class="section-7">
  <div class="container">
    <div class="row animated " data-animation="bounceInLeft">
      <section class="carousel-3">
        <div  class="carousel-title ">
          <div class="transform-please-2 "> <span> TRENDING NOW</span> </div>
        </div >
        <ul>
            <?php
            $args = array( 'posts_per_page' => 20, 'category_name'    => 'how-to' );
            $the_post = get_posts($args); 
            if($the_post): foreach($the_post as $post) : setup_postdata($post);?>
          <li>
            <div class="media">
                <?php if(has_post_thumbnail($post->ID)): ?>
                <?php $the_f_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                
                <a  href="<?php echo the_permalink(); ?>"><img src="<?php echo $the_f_image[0]; ?>" width="170" height="120" alt="alt"></a>
                <?php else: ?>
                <a  href="<?php echo the_permalink(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/media/product/small/item_01.jpg" width="170" height="120" alt="alt"></a>
            <?php endif; ?></div>
          
            <h3><?php the_title(); ?></h3>
            <p><?php echo the_excerpt(); ?></p>
            <div class="box-more-info">
              <div class="transform-revers"> <a href="#"><?php echo get_the_date(get_option('date_format')); ?></a></div>
            </div>
          </li>
        <?php endforeach; else: ?>
          <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                <?php wp_reset_postdata(); endif;?>
        </ul>
      </section>
    </div>
  </div>
</div>
<!-- END --> 
<div class="section-9">
  <div style="background: #fff;" class="bg3"></div>
  <div class="container">
    <div class="row">
      <section class="video-section animated " data-animation="bounceInRight">
          
        <div class="carousel-title ">
          <div class="transform-please-2 "> <span> VIDEO SECTION</span> </div>
        </div>
          
        <div class="col-xs-12 col-sm-4 col-lg-4">
          <article  class="clearfix animated text-left ">
            <ul class="nav nav-tabs verticale-tabs">
              <li class="active">
                <div class="active-panel">Weed Farmer Intro v1.0 <a href="#" class="arrow-link"> <span class="icon-transform transform-please-2"><i class="fa fa-angle-right"></i></span></a></div>
                <a href="#86024" data-toggle="tab"> <img width="550" height="344" alt="alt" src="<?php echo get_template_directory_uri() ; ?>/media/section6/1.jpg"></a></li>
            </ul>
          </article>
        </div>
          
        <div class="tab-content verticale-tabs-content col-xs-12 col-sm-8 col-lg-8">
          <div class="tab-pane active" id="86024">
            <article class="clearfix animated text-left ">
              <iframe width="560" height="315" src="https://www.youtube.com/embed/aN6CvqRAVd4" frameborder="0" allowfullscreen></iframe>
              <div class="desc-video">
                <p> DMC Aventador Molto Veloce LP900 </p>
              </div>
              <div class="box-more-info">
                <div class="transform-revers"> <a href="#"><i class="fa fa-clock-o"></i> JOHN DEO</a></div>
                <div class="transform-revers"> <a href="#"> <i class="fa fa-clock-o"></i> JULY 16 2020</a></div>
                <div class="transform-revers"> <a href="#"><i class="fa fa-comment-o"></i> 5 COMMENTS</a></div>
              </div>
            </article>
          </div>
        </div>
          
      </section>
    </div>
  </div>
</div>
<!-- END --> 

<!--END--> 

<section class="prefooter">
  <div class="container prefooter-container">
    <div id="logo-bottom" class=" animated "  data-animation="bounceInLeft"> <span class="bglogo1 transform-please-2"><a href="index.html"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="logo"></a></span> </div>
    <div class="title-line"></div>
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 animated "  data-animation="bounceInLeft">
        <p> Our community is about helping and improving your growing skills, as well as some funny pictures/videos, and let’s not forget our tips and tricks for your very own indoor/outdoor growing style. If you have any further questions, please feel free to message us, we will be sure to respond in the quickest way possible.</p>
        <a class="btn btn-main btn-primary btn-lg uppercase" href="#"><span><i class="fa fa-angle-right"></i>READ MORE</span></a> </div>
      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 box-prefoot animated "  data-animation="bounceInUp">
        <h4>BEST REVIEWS</h4>
        <ul class="entry-list unstyled">
            <?php 
                $r_args = array(
                    'posts_per_page'   => 3,
                    'category_name' => 'reviews',
                    'orderby' => 'comment_count'
                );
                $br_posts = get_posts($r_args);
            foreach($br_posts as $post):
            ?>
          <li>
            <div class="entry-thumbnail"> <a class="img" href="<?php the_permalink();?>"> <img width="70" height="70" alt="alt" src="<?php echo get_template_directory_uri();?>/media/small/1.jpg"> </a> </div>
            <div class="entry-main">
              <div class="entry-header">
                <h5 class="entry-title"><a href="#"><?php the_title();?></a></h5>
              </div>
              <div class="entry-meta">
                <time title="<?php echo get_the_date(get_option('date_format')); ?>" datetime="<?php echo get_the_date(get_option('date_format')); ?>" class="entry-datetime"> <a href="<?php the_permalink(); ?>"><span class="icon-clock" aria-hidden="true"></span> <?php echo get_the_date(get_option('date_format')); ?></a> </time>
              </div>
            </div>
            <div class="clearfix"></div>
          </li>
            <?php endforeach; ?>
        </ul>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 box-prefoot animated "  data-animation="bounceInUp">
        <h4>PHOTOS FROM FLICKR</h4>
        <div id="flickr-feed"></div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 box-prefoot animated "  data-animation="bounceInRight">
        <h4>LATEST TWEETS</h4>
        <ul class="tweet-box">
          <li>
            <div class="tw-icon"><i class="fa fa-twitter"></i></div>
            <div class="tweet-content"> Nulla euis se modm lesuada nibh cua
              bueitu de accumsan sem males.</div>
            <div class="tweet-datetime">1 HOUR AGO</div>
          </li>
          <li>
            <div class="tw-icon"><i class="fa fa-twitter"></i></div>
            <div class="tweet-content"> Nulla euis se modm lesuada nibh cua
              bueitu de accumsan sem males.</div>
            <div class="tweet-datetime">1 HOUR AGO</div>
          </li>
          <li>
            <div class="tw-icon"><i class="fa fa-twitter"></i></div>
            <div class="tweet-content"> Nulla euis se modm lesuada nibh cua
              bueitu de accumsan sem males.</div>
            <div class="tweet-datetime">1 HOUR AGO</div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>

<!-- END --> 
<!-- HOME SLIDER END --> 
<?php get_footer(); ?>