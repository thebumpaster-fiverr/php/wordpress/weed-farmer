<?php get_header(); ?>
<main id="main" class="section">
  <div class="container">
    <div class="row">
      <section class="carousel carousel-reviews">
        <div class="carousel-title ">
          <div class="transform-please-2 "> <span> STRAIN REVIEWS </span> </div>
        </div>
        <ul class="carousel-1">
            <?php 
            $args = array( 'posts_per_page' => 20, 'category_slug' => 'reviews' );
            $the_post = get_posts($args); 

            if($the_post): foreach($the_post as $post) : setup_postdata($post);  ?>
          <li>
            <div class="media">
                <?php if(has_post_thumbnail($post->ID)): ?>
                <?php $the_f_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                
                <a href="<?php the_permalink(); ?>"><img src="<?php echo $the_f_image[0]; ?>" width="369" height="200" alt="alt"/></a>
                <?php else : ?>
                    <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/media/blog/1.jpg" width="369" height="200" /> </a>
                <?php endif; ?>
              <div class="carousel-item-content">
                <div class="text-right"><a class="arrow-link" href="#"> <span class="icon-transform transform-please-2"><i class="fa fa-angle-right"></i></span></a></div>
                <a href="<?php the_permalink(); ?>" class="transform-please-2 carousel-title"><span><?php the_title(); ?></span> </a> </div>
            </div>
            <div class="carousel-text">
              <p><?php the_excerpt(); ?> </div>
            <div class="box-more-info">
              <div class="transform-revers"> <a href="#"><?php the_time('F jS, Y'); ?></a></div>
            </div>
          </li>
        <?php endforeach; else: ?>
	       <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                <?php wp_reset_postdata(); endif;?>
        </ul>
      </section>
      <div class="line transform-please-2"></div>
      
      <!-- MAIN CONTENT -->
      <div class="col-xs-12 col-sm-12 col-md-9">
        <section class="main-content" role="main">
          <article class="post format-image">
              <?php the_post();  $temporal = $post->ID;?>
            <div class="entry-header">
              <div class="box-more-info">
                <div class="transform-revers"> <a href="#"> <i class="fa fa-clock-o"></i> <?php echo get_the_date(get_option('date_format')); ?></a></div>
                <div class="transform-revers"> <a href="#"><i class="fa fa-comment-o"></i> 5 COMMENTS</a></div>
              </div>
            </div>
            <div class="entry-media">
              <div class="entry-thumbnail img">
                <div class="img-overlay "> </div>
                <a href="#"> 
                    <?php
                        if(has_post_thumbnail()):
                    ?>
                    <img src="<?php the_post_thumbnail_url( $post->ID ) ?>" width="870" height="300" alt=""/>
                    <?php else: ?>
                    <img src="<?php echo get_template_directory_uri(); ?>/media/blog/1.jpg" width="870" height="300" alt="No Feautured image" />
                    <?php endif; ?>
                  </a> 
                </div>
            </div>
            <div class="entry-main">
              <h3 class="entry-title"> <?php the_title(); ?></h3>
              <div class="entry-content">
                  <?php the_content(); ?>
              </div>
            </div>
            <div class="line-block transform-please-2"></div>
              <?php wp_reset_postdata();?>
          </article>
          <div class="comments-wrapper">
            <section class="post-list-mini related-post">
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 ">
                  <div class="comments-header"><span>RELATED POSTS</span> <a class="arrow-link" href="#"> <span class="icon-transform transform-please-2"><i class="fa fa-angle-right"></i></span></a> </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">
                  <ul>
                      <?php
                        $tags = wp_get_post_tags($post->ID);
                      
                        if ($tags):
                       
                        $first_tag = $tags[0]->term_id;
                       
                        $args=array(
                        'tag__in' => array($first_tag),
                        'post__not_in' => array($post->ID),
                        'posts_per_page'=>5,
                        'caller_get_posts'=>1
                        );
                            
                        $my_query = new WP_Query($args);
                            
                        if( $my_query->have_posts() ): 
                            
                        while ($my_query->have_posts()) : $my_query->the_post(); ?>
                      
                    <li class="col-lg-6 col-md-6 col-sm-6">
                      <div class="media col-lg-5 col-md-5 col-sm-5"> 
                          <?php if(has_post_thumbnail()): ?>
                          <a href="<?php the_post_thumbnail_url( $post->ID ) ?>"><img width="369" height="200" alt="alt" src="<?php the_post_thumbnail_url( $post->ID ) ?>"></a>
                          <?php else: ?>
                          <a href="#"><img width="369" height="200" alt="alt" src="<?php the_template_directory_uri() ?>/media/blog/1.jpg"></a>
                          <?php endif; ?>
                        </div>
                      <div class="media-body col-lg-7 col-md-7 col-sm-7"> <?php the_title(); ?></div>
                      <span class="transform-please"></span> 
                    </li>
                      <?php endwhile; else: ?>
                       <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                      <?php endif; wp_reset_query(); endif; ?>
                        
                  </ul>
                </div>
              </div>
            </section>
            <div class="line-block transform-please-2"></div>
            <section class="section-comment">
              <div class="comments-header"><span>Comments</span> <a class="arrow-link" href="#"> <span class="icon-transform transform-please-2"><i class="fa fa-angle-right"></i></span></a> </div>
              
              <!-- COMMENT LIST -->
                
                <?php
                    $comments = get_comments(array(
                        'post_id' => $temporal,
                        'status' => 'approve'
                    ));
                
                wp_list_comments(array(
                    'walker' => new TheBump_walker_comment,
                    'per_page' => 10,
                    'avatar_size' => 72,
                    'reverse_top_level' => false), $comments);
                ?>
              <!-- // COMMENT LIST -->
              
              <!-- COMMENT REPLY -->
                
                <?php
                $fields =  array(

            'author' =>
                '<p class="comment-form-author"><label for="author">' . __( 'Name', 'domainreference' ) . '</label> ' .
                ( $req ? '<span class="required">*</span>' : '' ) .
                '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) .
                '" size="30"' . $aria_req . ' /></p>',

            'email' =>
                '<p class="comment-form-email"><label for="email">' . __( 'Email', 'domainreference' ) . '</label> ' .
                ( $req ? '<span class="required">*</span>' : '' ) .
                '<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
                '" size="30"' . $aria_req . ' /></p>',

            'url' =>
                '<p class="comment-form-url"><label for="url">' . __( 'Website', 'domainreference' ) . '</label>' .
                '<input id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) .
                '" size="30" /></p>'
                );
                
                $c_arg = array(
                    'id_form' => 'comment-reply-form',
                    'class_form' => 'form-full-width',
                    'comment_notes_before' => '',
                    'title_reply' => '<div class="comments-header"><span>Leave a Comment</span> <a href="#" class="arrow-link"> <span class="icon-transform transform-please-2"><i class="fa fa-angle-right"></i></span></a> </div>',
                    'id_submit' => 'submit',
                    'class_submit' => 'btn btn-main btn-primary btn-lg uppercase',
                    'comment_field' => '<div class="form-group">
                      <textarea placeholder="Your Message" required name="comment" id="comment-text"></textarea>
                    </div>',
                    'fileds' => apply_filters('comment_form_default_fields', $fileds )
                    
                );
                comment_form($c_arg); 
                
                ?>
              
            </section>
          </div>
        </section>
      </div>
      <!-- // MAIN CONTENT -->
      <div class="space40 visible-xs"></div>
      <!-- SIDEBAR -->
      <?php get_sidebar(); ?>
      <!-- // SIDEBAR --> 
    </div>
  </div>
</main>

<?php get_footer(); ?>