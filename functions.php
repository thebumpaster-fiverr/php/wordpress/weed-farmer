<?php
	function add_theme_scripts() {
		
		
	wp_enqueue_style( 'style', get_stylesheet_uri() );
	wp_enqueue_style( 'BootStrap', get_template_directory_uri().'/css/bootstrap-3.1.1.min.css' );
	wp_enqueue_style( 'Custom', get_template_directory_uri().'/css/custom.css' );
	wp_enqueue_style( 'Inner', get_template_directory_uri().'/css/inner.css' );
	wp_enqueue_style( 'Home', get_template_directory_uri().'/css/home.css' );
	wp_enqueue_style( 'Shop', get_template_directory_uri().'/css/shop.css' );
	wp_enqueue_style( 'Responsive', get_template_directory_uri().'/css/responsive.css' );
	wp_enqueue_style( 'Color', get_template_directory_uri().'/css/color.css' );
	wp_enqueue_style( 'SmartMenu', get_template_directory_uri().'/css/smart-menu.css' );
	wp_enqueue_style( 'Animate', get_template_directory_uri().'/css/animate.css' );
	wp_enqueue_style( 'Debugg', get_template_directory_uri().'/css/debugging.css' );
	
	wp_enqueue_style( 'jQuery SelectBox', get_template_directory_uri().'/plugins/selectbox/jquery.selectbox.css' );
	wp_enqueue_style( 'Camera Slider', get_template_directory_uri().'/plugins/camera-slider/camera-slider-1.3.4.min.css' );
	wp_enqueue_style( 'FancyBox', get_template_directory_uri().'/plugins/fancybox/fancybox-1.3.4.min.css' );
	wp_enqueue_style( 'FancyBox PhotoSwipe', get_template_directory_uri().'/plugins/fancybox/photoswipe-3.0.5.min.css' );
	wp_enqueue_style( 'Hover Animation', get_template_directory_uri().'/plugins/hover-animations/hover-animations-1.0.min.css' );
	wp_enqueue_style( 'Animations', get_template_directory_uri().'/plugins/justinaguilar-animations/animations.css' );
	wp_enqueue_style( 'Validation', get_template_directory_uri().'/plugins/validation/validation-2.2.min.css' );
	wp_enqueue_style( 'BoxSlider', get_template_directory_uri().'/plugins/bxslider/jquery.bxslider.css' );
	wp_enqueue_style( 'NOUI Slider', get_template_directory_uri().'/plugins/nouislider/jquery.nouislider.css' );
	wp_enqueue_style( 'FLEX Slider', get_template_directory_uri().'/plugins/flexslider/flexslider.css' );

	wp_enqueue_script( 'jQuery Migrate', get_template_directory_uri().'/js/jquery.js' );
	wp_enqueue_script( 'jQuery Migrate', get_template_directory_uri().'/js/jquery-migrate-1.2.1.js' );
	wp_enqueue_script( 'BootStrap', get_template_directory_uri().'/js/bootstrap-3.1.1.min.js' );
	wp_enqueue_script( 'Modernizr', get_template_directory_uri().'/js/modernizr.custom.js' );
	wp_enqueue_script( 'Classie', get_template_directory_uri().'/js/classie.js' );
	wp_enqueue_script( 'Path Loader', get_template_directory_uri().'/js/pathLoader.js' );
	wp_enqueue_script( 'WayPoints', get_template_directory_uri().'/js/waypoints.min.js' );
	wp_enqueue_script( 'Clickr Feed', get_template_directory_uri().'/js/jflickrfeed.min.js' );
	wp_enqueue_script( 'SmartMenu', get_template_directory_uri().'/js/smart-menu.js' );
	wp_enqueue_script( 'CSSUA', get_template_directory_uri().'/js/cssua.min.js' );
	wp_enqueue_script( 'Custom', get_template_directory_uri().'/js/custom.js' );
	
	wp_enqueue_script( 'jQuery SelectBox', get_template_directory_uri().'/plugins/selectbox/jquery.selectbox-0.2.js' );
	wp_enqueue_script( 'jQuery BoxSlider', get_template_directory_uri().'/plugins/bxslider/jquery.bxslider.min.js' );
	wp_enqueue_script( 'jQuery NOUI Slider', get_template_directory_uri().'/plugins/nouislider/jquery.nouislider.min.js' );
	wp_enqueue_script( 'jQuery FLEX Slider', get_template_directory_uri().'/plugins/flexslider/jquery.flexslider.js' );
	wp_enqueue_script( 'BackStretch', get_template_directory_uri().'/plugins/backstretch/backstretch-2.0.4.min.js' );
	wp_enqueue_script( 'Camera Slider', get_template_directory_uri().'/plugins/camera-slider/camera-slider-1.3.4.min.js' );
	wp_enqueue_script( 'Camera Slider Easing', get_template_directory_uri().'/plugins/camera-slider/easing-1.3.min.js' );
	wp_enqueue_script( 'FancyBox', get_template_directory_uri().'/plugins/fancybox/fancybox-1.3.4.pack.js' );
	wp_enqueue_script( 'FancyBox Class', get_template_directory_uri().'/plugins/fancybox/klass-1.0.min.js' );
	wp_enqueue_script( 'FancyBox PhotoSwipe', get_template_directory_uri().'/plugins/fancybox/photoswipe-3.0.5.min.js' );
	wp_enqueue_script( 'Hover Animation 2D', get_template_directory_uri().'/plugins/hover-animations/transform2d.min.js' );
	wp_enqueue_script( 'Hover Animation', get_template_directory_uri().'/plugins/hover-animations/hover-animations-1.0.min.js' );
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );

function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' ),
      'extra-menu' => __( 'Extra Menu' ),
	  'footer-menu' => __('Footer menu')
    )
  );
}
add_action( 'init', 'register_my_menus' );


/**
 * Custom walker class.
 */
class WPDocs_Walker_Nav_Menu extends Walker_Nav_Menu {
 
    /**
     * Starts the list before the elements are added.
     *
     * Adds classes to the unordered list sub-menus.
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param int    $depth  Depth of menu item. Used for padding.
     * @param array  $args   An array of arguments. @see wp_nav_menu()
     */
    function start_lvl( &$output, $depth = 0, $args = array() ) {
        // Depth-dependent classes.
        $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
        $display_depth = ( $depth + 1); // because it counts the first submenu as 0
        $classes = array(
            'dropdown-menu',
            ( $display_depth % 2  ? 'menu-odd' : 'menu-even' ),
            ( $display_depth >=2 ? 'sub-sub-menu' : '' ),
            'menu-depth-' . $display_depth
        );
        $class_names = implode( ' ', $classes );
 
        // Build HTML for output.
        $output .= "\n" . $indent . '<ul class="' . $class_names . '">' . "\n";
    }
 
    /**
     * Start the element output.
     *
     * Adds main/sub-classes to the list items and links.
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param object $item   Menu item data object.
     * @param int    $depth  Depth of menu item. Used for padding.
     * @param array  $args   An array of arguments. @see wp_nav_menu()
     * @param int    $id     Current item ID.
     */
    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        global $wp_query;
        $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent
 
        // Depth-dependent classes.
        $depth_classes = array(
            ( $depth == 0 ? 'main-menu-item dropdown-toggle' : 'sub-menu-item' ),
            ( $depth >=2 ? 'sub-sub-menu-item' : '' ),
            ( $depth % 2 ? 'menu-item-odd' : 'menu-item-even' ),
            'menu-item-depth-' . $depth
        );
        $depth_class_names = esc_attr( implode( ' ', $depth_classes ) );
 
        // Passed classes.
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );
 
        // Build HTML.
        $output .= $indent . '<li id="nav-menu-item-'. $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . '">';
 
        // Link attributes.
        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
        $attributes .= ' class="menu-link ' . ( $depth > 0 ? 'sub-menu-link' : 'main-menu-link' ) . '"';
 
        // Build HTML output and pass through the proper filter.
        $item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
            $args->before,
            $attributes,
            $args->link_before,
            apply_filters( 'the_title', $item->title, $item->ID ),
            $args->link_after,
            $args->after
        );
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}


class FooterWidgetMenu extends WP_Widget {

	function __construct() {
		// Instantiate the parent object
		parent::__construct( false, 'The Footer Widget Menu' );
	}

	function widget( $args, $instance ) {
		// Widget output     
	}

	function update( $new_instance, $old_instance ) {
		// Save widget options
	}

	function form( $instance ) {
		// Output admin widget options form
	}
}

function myplugin_register_widgets() {
	register_widget( 'FooterWidgetMenu' );
}

add_action( 'widgets_init', 'myplugin_register_widgets' );
add_theme_support( 'post-thumbnails' ); 


/** COMMENTS WALKER */
class TheBump_walker_comment extends Walker_Comment {
	
	// init classwide variables
	var $tree_type = 'comment';
	var $db_fields = array( 'parent' => 'comment_parent', 'id' => 'comment_ID' );

	/** CONSTRUCTOR
	 * You'll have to use this if you plan to get to the top of the comments list, as
	 * start_lvl() only goes as high as 1 deep nested comments */
	function __construct() { ?>
		
		<ul id="comment-list" class="comments-list unstyled clearfix">
		
	<?php }
	
	/** START_LVL 
	 * Starts the list before the CHILD elements are added. Unlike most of the walkers,
	 * the start_lvl function means the start of a nested comment. It applies to the first
	 * new level under the comments that are not replies. Also, it appear that, by default,
	 * WordPress just echos the walk instead of passing it to &$output properly. Go figure.  */
	function start_lvl( &$output, $depth = 0, $args = array(), $id = 0 ) {		
		$GLOBALS['comment_depth'] = $depth + 1; ?>

				<ul class="children clearfix">
	<?php }

	/** END_LVL 
	 * Ends the children list of after the elements are added. */
	function end_lvl( &$output, $depth = 0, $args = array(), $id = 0 ) {
		$GLOBALS['comment_depth'] = $depth + 1; ?>

		</ul><!-- /.children -->
		
	<?php }
	
	/** START_EL */
	function start_el( &$output, $comment, $depth = 0, $args = array(), $id = 0 ) {
		$depth++;
		$GLOBALS['comment_depth'] = $depth;
		$GLOBALS['comment'] = $comment; 
		$parent_class = ( empty( $args['has_children'] ) ? '' : 'parent' ); ?>
		
		<li <?php comment_class( $parent_class ); ?> id="comment-<?php comment_ID() ?>">
            <article class="comment img">
                <div class="avatar-placeholder">
					<?php echo ( $args['avatar_size'] != 0 ? get_avatar( $comment, $args['avatar_size'] ) :'' ); ?>
				</div>
                <header class="comment-header">
                    <cite class="comment-author"><?php echo get_comment_author_link(); ?></cite>
                </header>
                
                <div id="comment-body-<?php comment_ID() ?>" class="comment-body">
            <!-- /.comment-author -->
					<?php if( !$comment->comment_approved ) : ?>
					<em class="comment-awaiting-moderation">Your comment is awaiting moderation.</em>
					
                    <?php else: ?><p> <?php comment_text(); ?></p>
					<?php endif; ?>
                    <!-- /.comment-content -->

				    <div class="transform-revers comment-reply">
                    <i class="fa fa-share"></i>
					<?php $reply_args = array(
						'add_below' => $add_below, 
						'depth' => $depth,
						'max_depth' => $args['max_depth'] );
	
					comment_reply_link( array_merge( $args, $reply_args ) );  ?>
				    </div><!-- /.reply -->
			     </div>
            </article><!-- /.comment-body -->

	<?php }

	function end_el(&$output, $comment, $depth = 0, $args = array(), $id = 0 ) { ?>
		
		</li><!-- /#comment-' . get_comment_ID() . ' -->
		
	<?php }
	
	/** DESTRUCTOR
	 * I just using this since we needed to use the constructor to reach the top 
	 * of the comments list, just seems to balance out :) */
	function __destruct() { ?>
	
	</ul><!-- /#comment-list -->

	<?php }
}

?>